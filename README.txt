======================================================================
=    Distributed History-Based Access Control Policy Evaluation      =
======================================================================

# OVERVIEW
Algorithm Based on Multiversion Concurrency Control with Timestamp Ordering
for Project Phases 3 and 4, an alternative algorithm for the problem in
[Decat+ 2015]. This algorithm is based on multiversion concurrency control
with timestamp ordering (MVCC-TSO).

## Installation:

DistAlgo stable version
Python 3.4 and above

## Running:

After installing DistAlgo, the system should run dac and dar command. If yes,
follwing will start the system according to config file and will log the
messages into log files kept at ./logs

    $ cd src
    $ make
    $ ./exec_test.sh i, where i is test number, e.g., i=1 will execute first test
    etc....

## Files:
From the root directory:
./src: contains all the source files
./src/applications.da # application code
./src/coordinator.da
./src/worker.da
./src/db.da
./src/staticfiles/policy.xml contains policy rules
./src/staticfiles/initial_db.xml contains iniitial database

All the logs and configuration files are in
./logs and ./configs respectively.

./testing.txt explains our detailed testing plan and config file format.

## Contributions:
We discussed the design together on paper and decided on modules and code
structure. Sridarshan coded subject and resource coordinator and vishal did
work on application and worker. We wrote the test plan together. Also, most
of the coding was done as pair coding, sitting together.

## Authors
Vishal Sahu
Sridarshan Shetty

# References

