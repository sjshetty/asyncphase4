set -v
if [ "$#" -lt 1 ]; then
    echo "Pass the test case num as arg to this script"
else
    cp ../config/testing/T"$1"/*.xml staticfiles/
    dar ../config/testing/T"$1"/makeConfigs_t1.da
    mv config_test"$1".cfg ../config/
    dar main.da ../config/config_test"$1".cfg
fi
