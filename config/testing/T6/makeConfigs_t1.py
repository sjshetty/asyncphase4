
import da
_config_object = {}
import configparser
import os
config = configparser.RawConfigParser()
config.add_section('cluster')
config.set('cluster', 'napps', '1')
config.set('cluster', 'ncoors', '2')
config.set('cluster', 'workerspercoor', '3')
config.add_section('staticfiles')
config.set('staticfiles', 'db', 'staticfiles/initial_db.xml')
config.set('staticfiles', 'policy', 'staticfiles/policy.xml')
config.add_section('application')
config.add_section('mightWriteObjs')
config.add_section('S_mightReadAttrs')
config.add_section('R_mightReadAttrs')
config.add_section('S_defReadAttrs')
config.add_section('R_defReadAttrs')
config.add_section('S_mightWriteAttrs')
config.add_section('R_mightWriteAttrs')
config.set('application', 'req_cnt', 2)
config.set('application', '0', '201,100,read,0,0,2')
config.set('mightWriteObjs', '0', '100')
config.set('S_mightReadAttrs', '0', 'position,history')
config.set('R_mightReadAttrs', '0', 'id')
config.set('application', 'delayBackReq', '0')
config.set('application', '1', '201,105,read,0,0,5')
config.set('mightWriteObjs', '1', '201')
config.set('S_mightReadAttrs', '1', 'position,history')
' config label logs specifies log files for each type of processes. This\nsection is read by all processes '
logDir = '../logs'
if (not os.path.exists(logDir)):
    os.makedirs(logDir)
config.add_section('logs')
config.set('logs', 'mainLogPath', '../logs/t6_main.log')
config.set('logs', 'appsLogPath', '../logs/t6_apps.log')
config.set('logs', 'coorsLogPath', '../logs/t6_coors.log')
config.set('logs', 'workerLogPath', '../logs/t6_worker.log')
" Writing our configuration file to 'configFile.cfg "
with open('config_test6.cfg', 'w') as configfile:
    config.write(configfile)
